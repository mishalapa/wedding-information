import Link from "next/link";
import Image from "next/image";
import logo from '../../public/images/logo-one-love-one-life-2-1024x1024.jpg'

export default function Header() {

    return (
        <header className='header'>
            <Link className='logo' href='./'>
                <Image className='logo-header' width={60} height={60} src={logo} alt='logo header'/>
            </Link>
            <ul className='wrapper-button-header'>
                <li>
                    <Link href='games'>
                        <button className='button-header'>
                            Список игр
                        </button>
                    </Link>
                </li>
                <li>
                    <Link href='guests'>
                        <button className='button-header'>
                            Список гостей
                        </button>
                    </Link>
                </li>

            </ul>
        </header>
    );
}
