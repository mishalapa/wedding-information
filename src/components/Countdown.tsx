import React, {useEffect, useState} from 'react';

interface CountdownState {
    days: number;
    hours: number;
    minutes: number;
    seconds: number;
}

const Countdown: React.FC = () => {
    const [countdown, setCountdown] = useState<CountdownState>({
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
    });

    useEffect(() => {
        const interval = setInterval(() => {
            const weddingDate = new Date('07/23/2023');
            const currentDate = new Date();
            // @ts-ignore
            const diff = weddingDate - currentDate;

            const days = Math.floor(diff / (1000 * 60 * 60 * 24));
            const hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
            const minutes = Math.floor((diff / 1000 / 60) % 60);
            const seconds = Math.floor((diff / 1000) % 60);

            setCountdown({days, hours, minutes, seconds});
        }, 1000);

        return () => clearInterval(interval);
    }, []);

    const addLeadingZero = (num: number): string => {
        return num < 10 ? `0${num}` : num.toString();
    };

    const isCountdownLoaded = countdown.days > 0 || countdown.hours > 0 || countdown.minutes > 0 || countdown.seconds > 0;

    return (
        <div className='timer-container'>
            <div
                className={isCountdownLoaded ? 'timer-digit' : 'timer-digit blur'}>{addLeadingZero(countdown.days)} Дней
            </div>
            <div
                className={isCountdownLoaded ? 'timer-digit' : 'timer-digit blur'}>{addLeadingZero(countdown.hours)} Часов
            </div>
            <div
                className={isCountdownLoaded ? 'timer-digit' : 'timer-digit blur'}>{addLeadingZero(countdown.minutes)} Минут
            </div>
            <div
                className={isCountdownLoaded ? 'timer-digit' : 'timer-digit blur'}>{addLeadingZero(countdown.seconds)} Секунд
            </div>
        </div>
    );
};

export default Countdown;