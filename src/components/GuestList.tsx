import React from 'react';

export interface Guest {
    id: number;
    firstName: string;
    lastName: string;
}

export interface GuestProps {
    guest: Guest;
}

const Guest: React.FC<GuestProps> = ({guest}) => {
    return (
        <div className='guest'>
            {guest.firstName} {guest.lastName}
        </div>
    );
};

interface GuestListProps {
    guests: Guest[];
}

export const GuestList: React.FC<GuestListProps> = ({guests}) => {
    return (
        <div className='wrapper-guests-one'>
            <div className='wrapper-guests'>
                {guests.map((guest) => (
                    <Guest key={guest.id} guest={guest}/>
                ))}
            </div>
        </div>
    );
};


export default GuestList;