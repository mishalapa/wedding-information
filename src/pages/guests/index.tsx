import GuestList, {Guest} from "@/components/GuestList";

export default function GuestsDetails() {
    const guests: Guest[] = [
        {id: 1, firstName: 'Лидия', lastName: 'Островская'},
        {id: 2, firstName: 'Людмила', lastName: 'Кароль'},
        {id: 3, firstName: 'Павел', lastName: 'Кароль'},
        {id: 4, firstName: 'Вова', lastName: 'Островский'},
        {id: 5, firstName: 'Оля', lastName: 'Островская'},
        {id: 6, firstName: 'Саша', lastName: 'Островская'},
        {id: 7, firstName: 'Аня', lastName: 'Чёрная'},
        {id: 8, firstName: 'Женя', lastName: 'Логвин'},
        {id: 9, firstName: 'Антон', lastName: 'Котов'},
        {id: 10, firstName: 'Настя', lastName: 'Олейниченко'},
        {id: 11, firstName: 'Паша', lastName: 'Гулидов'},
        {id: 12, firstName: 'Лиза', lastName: 'Морделёва'},
        {id: 13, firstName: 'Даша', lastName: 'Еськова'},
        {id: 14, firstName: 'Витя', lastName: 'Писаренок'},
        {id: 15, firstName: 'Даша', lastName: 'Писаренок'},
        {id: 16, firstName: 'Федя', lastName: 'Шугай'},
        {id: 17, firstName: 'Ксения', lastName: 'Седова'},
        {id: 18, firstName: 'Рита', lastName: 'Ивановская'},
        {id: 19, firstName: 'Антон', lastName: 'Циркин'},
        {id: 20, firstName: 'Евгений', lastName: 'Коробов'},
        {id: 21, firstName: 'Элизабет', lastName: 'Гантимурова'},
        {id: 22, firstName: 'Женя', lastName: 'Рябиченко'},
        {id: 23, firstName: 'Настя', lastName: 'Тишалович'},
        {id: 24, firstName: 'Настя', lastName: 'Островская'},
    ];

    const guestsOne: Guest[] = [
        {id: 1, firstName: 'Павел', lastName: 'Лапушинский'},
        {id: 2, firstName: 'Вероника', lastName: 'Кароль'},
        {id: 3, firstName: 'Кристиан', lastName: 'Лапушинский'},
        {id: 4, firstName: 'Аня', lastName: 'Боровко'},
        {id: 5, firstName: 'Андрей', lastName: 'Боровко'},
        {id: 6, firstName: 'Влад', lastName: 'Боровко'},
        {id: 7, firstName: 'Максим', lastName: 'Боровко'},
        {id: 8, firstName: 'Саша', lastName: 'Плыгавко'},
        {id: 9, firstName: 'Катя', lastName: 'Зубарева'},
        {id: 10, firstName: 'Артем', lastName: 'Козакевич'},
        {id: 11, firstName: 'Денис', lastName: 'Попов'},
        {id: 12, firstName: 'Оля', lastName: 'Попова'},
        {id: 13, firstName: 'Дима', lastName: 'Мандрук'},
        {id: 14, firstName: 'Аня', lastName: 'Мандрук'},
        {id: 15, firstName: 'Саша', lastName: 'Лабушев'},
        {id: 16, firstName: 'Маша', lastName: 'Лабушева'},
        {id: 17, firstName: 'Альфон', lastName: 'Лапушинский'},
        {id: 18, firstName: 'Алла', lastName: 'Лапушинская'},
        {id: 19, firstName: 'Валера', lastName: 'Шишкин'},
        {id: 20, firstName: 'Жанна', lastName: 'Шишкина'},
        {id: 21, firstName: 'Валера', lastName: 'Ковалев'},
        {id: 22, firstName: 'Вероника', lastName: 'Ковалева'},
        {id: 23, firstName: 'Ева', lastName: 'Ковалева'},
        {id: 24, firstName: 'Наталья', lastName: 'Островская'},
    ];

    const guestsTwo: Guest[] = [
        {id: 1, firstName: 'Игорь', lastName: 'Чечукович'},
        {id: 2, firstName: 'Татьяна', lastName: 'Чечукович'},
        {id: 3, firstName: 'Иоанн', lastName: 'Солопенко'},
        {id: 4, firstName: 'Александра', lastName: 'Солопенко'},
        {id: 5, firstName: 'Михаил', lastName: 'Лапушинский'},
        {id: 6, firstName: 'Екатерина', lastName: 'Лапушинская'},
        {id: 7, firstName: 'Таня', lastName: 'Лапушинская'},
        {id: 8, firstName: 'Сергей', lastName: 'Лапушинский'},
        {id: 9, firstName: 'Денис', lastName: 'Болваненко'},
        {id: 10, firstName: 'Саша', lastName: 'Гушляк'},
        {id: 11, firstName: 'Кристина', lastName: 'Бут'},
        {id: 12, firstName: 'Саша', lastName: 'Апанасёнок'},
        {id: 13, firstName: 'Лолита', lastName: 'Волохович'},
        {id: 14, firstName: 'Дима', lastName: 'Волохович'},
        {id: 15, firstName: 'Елена', lastName: 'Емельянова'},
        {id: 16, firstName: 'Наташа', lastName: 'Лазаревич'},
        {id: 17, firstName: 'Оля', lastName: 'Андреева'},
        {id: 18, firstName: 'Леша', lastName: 'Емельянов'},
        {id: 19, firstName: 'Маша', lastName: 'Шенделева'},
        {id: 20, firstName: 'Ваня', lastName: 'Шенделев'},
        {id: 21, firstName: 'Лиля', lastName: 'Баклаева'},
        {id: 22, firstName: 'Сергей', lastName: 'Баклаев'},
        {id: 23, firstName: 'София', lastName: 'Баклаева'},
        {id: 24, firstName: 'Валентин', lastName: 'Баклаев'},


    ];
    return (<div className='wrapper-guests-column'>
            <GuestList guests={guests}/>
            <GuestList guests={guestsOne}/>
            <GuestList guests={guestsTwo}/>
        </div>


    );
}