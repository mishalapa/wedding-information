import Image, {StaticImageData} from "next/image";
import zamki from '../../../public/images/big_box.png';
import sherif from '../../../public/images/ddd.jpg';
import inish from '../../../public/images/photo.jpg'
import catan from '../../../public/images/normal-22261346306.jpg'
import agricola from '../../../public/images/agrikola-front.png'
import ages from '../../../public/images/ages_box.jpg'
import hanseaticLeague from '../../../public/images/hanseaticLeague.jpg'
import wonders from '../../../public/images/7wonder1.png'
import contact from '../../../public/images/contact.webp'
import tiran from '../../../public/images/tir.jpg'
import azul from '../../../public/images/azul.png'
import orlean from '../../../public/images/orlean.jpg'

import {useMemo} from "react";
import Link from "next/link";

type LinkGame = {
    name: string,
    link: string,
}

interface IGames {
    id: number;
    name: string;
    description: string;
    logo: StaticImageData;
    linksBuy: LinkGame[];
    isBooked?: boolean;
}

interface IStore {
    id: number;
    name: string;
    description: string;
}


export default function GamesDetails() {

    const arrayGames = useMemo<IGames[]>(() => [
        {
            id: 8,
            name: 'Ганзейский союз',
            description: 'Во времена средневековья на севере Европы протянулись пути торгового Ганзейского союза, создавшего великолепные условия для развития предпринимателей и торговых гильдий',
            logo: hanseaticLeague,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/ganzeyskiy-soyuz/',
                },
            ]
        },
        {
            id: 4,
            name: 'Агрикола',
            description: 'Это игра о возрождении европейского населения в конце XVII века, после нескольких столетий эпидемий чумы.',
            logo: agricola,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/agrikola_novoe_izdanie/',
                },
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/agrikola-novoe-izdanie'
                },
            ]
        },
        {
            id: 9,
            name: 'Иниш',
            description: 'Кельтская культура таит в себе множество легенд, складывающихся в красивые эпосы.',
            logo: inish,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/inish/',
                },
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/inish'
                },
            ]
        },
        {
            id: 11,
            name: 'Космический контакт',
            description: 'На ваш выбор есть 51 раса пришельцев — от клонов и зомби до амёб и паразитов, — и каждая со своими уникальными суперсилами, которые помогут вашей империи разрастись до другого края галактики. ',
            logo: contact,
            linksBuy: [
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/kosmicheskij-kontakt'
                },
            ]
        },
        {
            id: 5,
            name: 'Шериф Ноттингема',
            description: 'В игре «Шериф Ноттингема» игроки становятся торговцами, желающими доставить свои товары на рынок.',
            logo: sherif,
            linksBuy: [
                {
                    name: 'Lavka Games.',
                    link: 'https://www.lavkaigr.ru/shop/igry-dlya-vecherinok/sherif-nottingema-pervaya-redaktsiya/'
                },
            ],
            isBooked: true,
        },
        {
            id: 3,
            name: '7 чудес',
            description: 'Все слышали о семи чудесах света древнего мира, но к сожалению до нашего времени "дожило" только одно из них. Эта игра позволит вам не только увидеть эти чудеса, но и приложить свою руку к их созданию!',
            logo: wonders,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/7-chudes-7-wonders-novoe-izdanie/',
                },
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/7-chudes-product-48455'
                },
            ],
            isBooked: true,
        },
        {
            id: 6,
            name: 'Сквозь Века',
            description: 'В этой сбалансированной стратегии вам предстоит вести свой народ (изначально примитивный, но очень жаждущий развиваться) через 4 эпохи - от глубокой древности до эры информации.',
            logo: ages,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/skvoz-veka/',
                },
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/skvoz-veka-novaja-istorija-civilizacii'
                },
            ],
            isBooked: true,
        },
        {
            id: 1,
            name: 'Колонизаторы',
            description: 'Серия "Колонизаторы" – одна из самых известных, интересных и оригинальных семейных настольных игр в мире. Всемирную известность она получила ещё в далёком 1995 году, когда получила престижную премию Spiel des Jahres.',
            logo: catan,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/kolonizatory/',
                },
                {
                    name: 'OZ',
                    link: 'https://oz.by/boardgames/more1093651.html'
                },
            ],
            isBooked: true,
        },
        {
            id: 10,
            name: 'Замки Бургундии',
            description: 'События «Замков Бургундии» происходят в регионе средневековой Франции. Игроки, выступая в роли аристократов, правят небольшими княжествами, застраивая те поселениями и прочными замками, практикуя речную торговлю, разрабатывая шахты и используя знания странников.',
            logo: zamki,
            linksBuy: [
                {
                    name: 'Empik(Польша)',
                    link: 'https://www.empik.com/rebel-gra-ekonomiczna-zamki-burgundii-big-box-rebel,p1245543244,zabawki-p',
                },
            ],
            isBooked: true,
        },
        {
            id: 7,
            name: 'Азул',
            description: 'Игроки по очереди набирают цветную плитку, всякий раз беря её по одному цвету из предлагаемых комплектов, чтобы раньше соперников выполнить заказ по облицовке дворца...',
            logo: azul,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/azul/',
                },
                {
                    name: 'Hobby Games',
                    link: 'https://hobbygames.by/azul'
                },
            ],
            isBooked: true,
        },
        {
            id: 2,
            name: 'Тираны Подземья',
            description: 'Тираны Подземья – стратегическая игра, в которой участники во главе благородных домов тёмных эльфов будут бороться за власть.',
            logo: tiran,
            linksBuy: [
                {
                    name: 'Lavka Games(Россия)',
                    link: 'https://www.lavkaigr.ru/shop/strategicheskie/tirany-podzemya/'
                },
            ],
            isBooked: true,
        },
        {
            id: 12,
            name: 'Орлеан',
            description: 'Орлеан – это насыщенная стратегия с многообразием игровых компонентов, действий и путей развития. Вся игра окутана гонкой за первенство, страсти кипят от начала до самого финала.',
            logo: orlean,
            linksBuy: [
                {
                    name: 'Знаем Играем',
                    link: 'https://znaemigraem.by/catalog/orlean/',
                },
                {
                    name: 'OZ',
                    link: 'https://oz.by/boardgames/more10932468.html'
                },
            ],
            isBooked: true,
        },
    ], []);

    const discountStore = useMemo<IStore[]>(() => [
        {
            id: 1,
            name: "Магазин «Знаем играем» — 15% znaemigraem.by",
            description: 'Просто скажите менеджеру, что вы из барахолки. Вы автоматически получите 15% скидку. При заказе через сайт укажите промокод: SL-0ZRPZ-MR2H29Y'

        },
    ], []);


    return (
        <div className="wrapper-page-games">
            <div className="wrapper-two-discount">
                <div className="title-discount">Скидки в наших любимых магазинах</div>
                <div className="wrapper-discount">
                    {
                        discountStore.map((item) => (
                            <div key={item.id}>
                                <div className="name-text">{item.name}</div>
                                <div className="description-text">{item.description}</div>
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className='wrapper-games'>
                {arrayGames.map((item, index) => (
                    <div className='game' key={item.id}>
                        <h2 className='name-game'>{item.name}</h2>
                        <Image className='image-games' src={item.logo} width={300} height={300} alt='games'/>
                        <div className="game-button">
                            <div className='title-game'>
                                <p className='name-description' style={{fontWeight: 700}}>Описание игры:</p>
                                <p className="text-description">{item.description}</p>
                            </div>
                            {item.isBooked ? "" : (
                                <p className='name-description' style={{marginTop: 10}}>Купить игру можно
                                    перейдя по ссылке:</p>)}
                            {item.isBooked ? (<button className="button-booked">Забронировано</button>) : (
                                <ul className='link-games-wrapper'>
                                    {item.linksBuy.map((item) => (
                                        <li className='li-link' style={{marginTop: 5}} key={item.name}>
                                            <Link className='link-games' href={item.link} target='_blank'>
                                                <p>{item.name}</p>
                                            </Link>
                                        </li>
                                    ))}
                                </ul>)}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}